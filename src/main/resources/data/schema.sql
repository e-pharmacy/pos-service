create table sales
(
    id                   BIGSERIAL PRIMARY KEY               NOT NULL,
    quantity             INTEGER                             NOT NULL,
    pharmacy_location_id BIGINT,
    product_id           BIGINT                              NOT NULL,
    sell_price_per_unit  DECIMAL                             NOT NULL,
    created_date           TIMESTAMP DEFAULT current_timestamp NOT NULL,
    created_by           BIGINT
);