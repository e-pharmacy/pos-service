/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 03/10/2021.
 */

package com.ubt.pos.commons.builders;

import com.ubt.pos.commons.models.BranchModel;
import com.ubt.pos.commons.models.ProductModel;
import com.ubt.pos.commons.models.SaleModel;

public final class SaleModelBuilder {
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdBy;
  private Integer quantity;
  private Long pharmacyLocationId;
  private BranchModel branchModel;
  private ProductModel productModel;
  private Long productId;
  private Integer salesCount;
  private Double sellPricePerUnit;

  private SaleModelBuilder() {
  }

  public static SaleModelBuilder aSaleModel() {
    return new SaleModelBuilder();
  }

  public SaleModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public SaleModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public SaleModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public SaleModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public SaleModelBuilder withQuantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  public SaleModelBuilder withPharmacyLocationId(Long pharmacyLocationId) {
    this.pharmacyLocationId = pharmacyLocationId;
    return this;
  }

  public SaleModelBuilder withBranchModel(BranchModel branchModel) {
    this.branchModel = branchModel;
    return this;
  }

  public SaleModelBuilder withProductModel(ProductModel productModel) {
    this.productModel = productModel;
    return this;
  }

  public SaleModelBuilder withProductId(Long productId) {
    this.productId = productId;
    return this;
  }

  public SaleModelBuilder withSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
    return this;
  }

  public SaleModelBuilder withSellPricePerUnit(Double sellPricePerUnit) {
    this.sellPricePerUnit = sellPricePerUnit;
    return this;
  }

  public SaleModel build() {
    SaleModel saleModel = new SaleModel();
    saleModel.setId(id);
    saleModel.setCreatedDate(createdDate);
    saleModel.setUpdatedDate(updatedDate);
    saleModel.setCreatedBy(createdBy);
    saleModel.setQuantity(quantity);
    saleModel.setPharmacyLocationId(pharmacyLocationId);
    saleModel.setBranchModel(branchModel);
    saleModel.setProductModel(productModel);
    saleModel.setProductId(productId);
    saleModel.setSalesCount(salesCount);
    saleModel.setSellPricePerUnit(sellPricePerUnit);
    return saleModel;
  }
}
