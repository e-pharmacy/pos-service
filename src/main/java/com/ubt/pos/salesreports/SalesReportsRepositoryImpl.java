/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.salesreports;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.salesreports.models.MonthlySaleModel;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

import static com.ubt.pos.jooq.Tables.SALES;

@Repository
public class SalesReportsRepositoryImpl implements SalesReportsRepository {
  private final DSLContext create;

  @Autowired
  public SalesReportsRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public List<SaleModel> getTop5ProductSalesFromLastMonth() {
    LocalDate firstDayOfLastMonth = YearMonth.now().atDay(1);

    return create.select(SALES.PRODUCT_ID, DSL.sum(SALES.QUANTITY).as("salesCount"))
        .from(SALES)
        .where(SALES.CREATED_AT.greaterOrEqual(firstDayOfLastMonth.atStartOfDay()))
        .groupBy(SALES.PRODUCT_ID)
        .fetchInto(SaleModel.class);
  }

  @Override
  public List<SaleModel> getBranchSalesFromLastMonth() {
    LocalDate firstDayOfLastMonth = YearMonth.now().atDay(1);

    return create.select(SALES.PHARMACY_LOCATION_ID, DSL.sum(SALES.QUANTITY).as("salesCount"))
        .from(SALES)
        .where(SALES.CREATED_AT.greaterOrEqual(firstDayOfLastMonth.atStartOfDay()))
        .groupBy(SALES.PHARMACY_LOCATION_ID)
        .fetchInto(SaleModel.class);
  }

  @Override
  public List<MonthlySaleModel> getMonthlySalesFromLastYear() {
    LocalDateTime firstDayOfCurrentYear = LocalDateTime.now().with(TemporalAdjusters.firstDayOfYear());

    return create.select(SALES.CREATED_AT, SALES.QUANTITY.as("salesCount"))
        .from(SALES)
        .where(SALES.CREATED_AT.ge(firstDayOfCurrentYear))
        .orderBy(SALES.CREATED_AT.asc())
        .fetchInto(MonthlySaleModel.class);
  }

  @Override
  public List<SaleModel> getMonthlySalesProfitFromLastYear() {
    LocalDateTime firstDayOfCurrentYear = LocalDateTime.now().with(TemporalAdjusters.firstDayOfYear());

    return create.select(SALES.asterisk())
        .from(SALES)
        .where(SALES.CREATED_AT.ge(firstDayOfCurrentYear))
        .fetchInto(SaleModel.class);
  }
}
