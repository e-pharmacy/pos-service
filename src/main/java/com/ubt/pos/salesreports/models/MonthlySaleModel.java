/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 16/09/2021.
 */

package com.ubt.pos.salesreports.models;

import com.ubt.pos.commons.models.BoBaseModel;

public class MonthlySaleModel extends BoBaseModel {
  public Integer salesCount;
  public Double profit;
  private String month;

  public Double getProfit() {
    return profit;
  }

  public void setProfit(Double profit) {
    this.profit = profit;
  }

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public Integer getSalesCount() {
    return salesCount;
  }

  public void setSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
  }
}
