/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.salesreports;

import com.ubt.pos.clients.MedicineFeignClient;
import com.ubt.pos.clients.StockFeignClient;
import com.ubt.pos.commons.models.BoBaseModel;
import com.ubt.pos.commons.models.BranchModel;
import com.ubt.pos.commons.models.ProductModel;
import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.salesreports.builders.MonthlySaleModelBuilder;
import com.ubt.pos.salesreports.models.MonthlySaleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class SalesReportsServiceImpl implements SalesReportsService {
  private final SalesReportsRepository salesReportsRepository;
  private final MedicineFeignClient medicineFeignClient;
  private final StockFeignClient stockFeignClient;

  @Autowired
  public SalesReportsServiceImpl(SalesReportsRepository salesReportsRepository, MedicineFeignClient medicineFeignClient, StockFeignClient stockFeignClient) {
    this.salesReportsRepository = salesReportsRepository;
    this.medicineFeignClient = medicineFeignClient;
    this.stockFeignClient = stockFeignClient;
  }

  @Override
  public List<SaleModel> getTop5ProductSalesFromLastMonth() {
    List<SaleModel> saleModelList = salesReportsRepository.getTop5ProductSalesFromLastMonth();

    //Get products models
    List<Long> productIds = saleModelList.stream().map(SaleModel::getProductId).collect(Collectors.toList());
    List<ProductModel> productsList = medicineFeignClient.getProducts(productIds, null, true);
    Map<Long, ProductModel> productsMap = productsList.stream().collect(Collectors.toMap(BoBaseModel::getId, e -> e));

    //Expand product objects
    saleModelList.forEach(saleModel -> saleModel.setProductModel(productsMap.get(saleModel.getProductId())));

    return saleModelList.stream().limit(5).collect(Collectors.toList());
  }

  @Override
  public List<SaleModel> getBranchSalesFromLastMonth() {
    List<SaleModel> saleModelList = salesReportsRepository.getBranchSalesFromLastMonth();

    //Get products models
    List<Long> branchesIds = saleModelList.stream().map(SaleModel::getPharmacyLocationId).collect(Collectors.toList());
    List<BranchModel> branchesList = stockFeignClient.getBranches(branchesIds);
    Map<Long, BranchModel> branchesMap = branchesList.stream().collect(Collectors.toMap(BoBaseModel::getId, e -> e));

    //Expand branch objects
    saleModelList.forEach(saleModel -> saleModel.setBranchModel(branchesMap.get(saleModel.getPharmacyLocationId())));

    return saleModelList;
  }

  @Override
  public List<MonthlySaleModel> getMonthlySalesFromLastYear() {
    List<MonthlySaleModel> salesList = salesReportsRepository.getMonthlySalesFromLastYear();

    Map<Integer, List<MonthlySaleModel>> salesListGroupedByMonth = salesList.stream()
        .collect(groupingBy(e -> {
          Calendar cal = Calendar.getInstance();
          cal.setTimeInMillis(e.getCreatedAt());
          return cal.get(Calendar.MONTH) + 1;
        }));

    List<MonthlySaleModel> monthlyProfitSalesList = initalizateEmptyListWithAllMonthsOfThisYear();
    for (Integer key : salesListGroupedByMonth.keySet()) {
      List<MonthlySaleModel> monthlySalesOfThisMonth = salesListGroupedByMonth.get(key);
      Integer salesCount = monthlySalesOfThisMonth.stream().map(MonthlySaleModel::getSalesCount).reduce(Integer::sum).get();

      switch (key) {
        case 1:
          monthlyProfitSalesList.get(0).setSalesCount(salesCount);
          break;
        case 2:
          monthlyProfitSalesList.get(1).setSalesCount(salesCount);
          break;
        case 3:
          monthlyProfitSalesList.get(2).setSalesCount(salesCount);
          break;
        case 4:
          monthlyProfitSalesList.get(3).setSalesCount(salesCount);
          break;
        case 5:
          monthlyProfitSalesList.get(4).setSalesCount(salesCount);
          break;
        case 6:
          monthlyProfitSalesList.get(5).setSalesCount(salesCount);
          break;
        case 7:
          monthlyProfitSalesList.get(6).setSalesCount(salesCount);
          break;
        case 8:
          monthlyProfitSalesList.get(7).setSalesCount(salesCount);
          break;
        case 9:
          monthlyProfitSalesList.get(8).setSalesCount(salesCount);
          break;
        case 10:
          monthlyProfitSalesList.get(9).setSalesCount(salesCount);
          break;
        case 11:
          monthlyProfitSalesList.get(10).setSalesCount(salesCount);
          break;
        case 12:
          monthlyProfitSalesList.get(11).setSalesCount(salesCount);
          break;
      }
    }

    //get the list with only latest 6 months objects
    if (monthlyProfitSalesList.size() > 6) {
      return monthlyProfitSalesList.subList(monthlyProfitSalesList.size() - 6, monthlyProfitSalesList.size());
    }

    return monthlyProfitSalesList;
  }

  @Override
  public List<MonthlySaleModel> getMonthlySalesProfitFromLastYear() {
    List<SaleModel> salesList = salesReportsRepository.getMonthlySalesProfitFromLastYear();

    Map<Integer, List<SaleModel>> salesListGroupedByMonth = salesList.stream()
        .collect(groupingBy(e -> {
          Calendar cal = Calendar.getInstance();
          cal.setTimeInMillis(e.getCreatedAt());
          return cal.get(Calendar.MONTH) + 1;
        }));

    List<MonthlySaleModel> monthlyProfitSalesList = initalizateEmptyListWithAllMonthsOfThisYear();
    for (Integer key : salesListGroupedByMonth.keySet()) {
      List<SaleModel> saleModel = salesListGroupedByMonth.get(key);
      Double profit = saleModel.stream().map(e -> e.getSellPricePerUnit() * e.getQuantity()).reduce(Double::sum).get();

      switch (key) {
        case 1:
          monthlyProfitSalesList.get(0).setProfit(profit);
          break;
        case 2:
          monthlyProfitSalesList.get(1).setProfit(profit);
          break;
        case 3:
          monthlyProfitSalesList.get(2).setProfit(profit);
          break;
        case 4:
          monthlyProfitSalesList.get(3).setProfit(profit);
          break;
        case 5:
          monthlyProfitSalesList.get(4).setProfit(profit);
          break;
        case 6:
          monthlyProfitSalesList.get(5).setProfit(profit);
          break;
        case 7:
          monthlyProfitSalesList.get(6).setProfit(profit);
          break;
        case 8:
          monthlyProfitSalesList.get(7).setProfit(profit);
          break;
        case 9:
          monthlyProfitSalesList.get(8).setProfit(profit);
          break;
        case 10:
          monthlyProfitSalesList.get(9).setProfit(profit);
          break;
        case 11:
          monthlyProfitSalesList.get(10).setProfit(profit);
          break;
        case 12:
          monthlyProfitSalesList.get(11).setProfit(profit);
          break;
      }
    }

    return monthlyProfitSalesList;
  }

  private List<MonthlySaleModel> initalizateEmptyListWithAllMonthsOfThisYear() {
    List<MonthlySaleModel> monthlySaleModelList = new ArrayList<>();
    int monthsAdded = 0;

    //get current month number.
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(System.currentTimeMillis());
    int currentMonthIndex = cal.get(Calendar.MONTH);

    monthlySaleModelList.add(
        MonthlySaleModelBuilder.aMonthlySaleModel()
            .withMonth("Jan")
            .withSalesCount(0)
            .withProfit(0.0)
            .build());

    if (currentMonthIndex >= 1) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Feb")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 2) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Mar")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }


    if (currentMonthIndex >= 3) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Apr")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }


    if (currentMonthIndex >= 4) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("May")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }


    if (currentMonthIndex >= 5) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Jun")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 6) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Jul")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 7) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Aug")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 8) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Sep")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 9) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Oct")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 10) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Nov")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    if (currentMonthIndex >= 11) {
      monthlySaleModelList.add(
          MonthlySaleModelBuilder.aMonthlySaleModel()
              .withMonth("Dec")
              .withSalesCount(0)
              .withProfit(0.0)
              .build());
    }

    return monthlySaleModelList;
  }
}
