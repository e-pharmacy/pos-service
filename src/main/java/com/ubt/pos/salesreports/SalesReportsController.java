/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.salesreports;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.salesreports.models.MonthlySaleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/sales/reports")
public class SalesReportsController {
  private final SalesReportsService salesReportsService;

  @Autowired
  public SalesReportsController(SalesReportsService salesReportsService) {
    this.salesReportsService = salesReportsService;
  }

  @GetMapping(params = {"reports=TopProductSales"})
  public List<SaleModel> getTop5ProductSalesFromLastMonth() {
    return salesReportsService.getTop5ProductSalesFromLastMonth();
  }

  @GetMapping(params = {"reports=branchSales"})
  public List<SaleModel> getBranchSalesFromLastMonth() {
    return salesReportsService.getBranchSalesFromLastMonth();
  }

  @GetMapping(params = {"reports=monthlySales"})
  public List<MonthlySaleModel> getMonthlySalesFromLastYear() {
    return salesReportsService.getMonthlySalesFromLastYear();
  }

  @GetMapping(params = {"reports=monthlySalesProfit"})
  public List<MonthlySaleModel> getMonthlySalesProfitFromLastYear() {
    return salesReportsService.getMonthlySalesProfitFromLastYear();
  }
}