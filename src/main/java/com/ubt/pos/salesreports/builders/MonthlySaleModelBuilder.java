/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 05/10/2021.
 */

package com.ubt.pos.salesreports.builders;

import com.ubt.pos.salesreports.models.MonthlySaleModel;

public final class MonthlySaleModelBuilder {
  public Integer salesCount;
  public Double profit;
  private Long id;
  private Long createdDate;
  private Long updatedDate;
  private Long createdAt;
  private Long updatedAt;
  private Long createdBy;
  private String month;

  private MonthlySaleModelBuilder() {
  }

  public static MonthlySaleModelBuilder aMonthlySaleModel() {
    return new MonthlySaleModelBuilder();
  }

  public MonthlySaleModelBuilder withId(Long id) {
    this.id = id;
    return this;
  }

  public MonthlySaleModelBuilder withCreatedDate(Long createdDate) {
    this.createdDate = createdDate;
    return this;
  }

  public MonthlySaleModelBuilder withUpdatedDate(Long updatedDate) {
    this.updatedDate = updatedDate;
    return this;
  }

  public MonthlySaleModelBuilder withCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  public MonthlySaleModelBuilder withUpdatedAt(Long updatedAt) {
    this.updatedAt = updatedAt;
    return this;
  }

  public MonthlySaleModelBuilder withCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
    return this;
  }

  public MonthlySaleModelBuilder withSalesCount(Integer salesCount) {
    this.salesCount = salesCount;
    return this;
  }

  public MonthlySaleModelBuilder withProfit(Double profit) {
    this.profit = profit;
    return this;
  }

  public MonthlySaleModelBuilder withMonth(String month) {
    this.month = month;
    return this;
  }

  public MonthlySaleModel build() {
    MonthlySaleModel monthlySaleModel = new MonthlySaleModel();
    monthlySaleModel.setId(id);
    monthlySaleModel.setCreatedDate(createdDate);
    monthlySaleModel.setUpdatedDate(updatedDate);
    monthlySaleModel.setCreatedAt(createdAt);
    monthlySaleModel.setUpdatedAt(updatedAt);
    monthlySaleModel.setCreatedBy(createdBy);
    monthlySaleModel.setSalesCount(salesCount);
    monthlySaleModel.setProfit(profit);
    monthlySaleModel.setMonth(month);
    return monthlySaleModel;
  }
}
