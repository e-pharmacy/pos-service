/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.salesreports;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.salesreports.models.MonthlySaleModel;

import java.util.List;

public interface SalesReportsService {
  List<SaleModel> getTop5ProductSalesFromLastMonth();

  List<SaleModel> getBranchSalesFromLastMonth();

  List<MonthlySaleModel> getMonthlySalesFromLastYear();

  List<MonthlySaleModel> getMonthlySalesProfitFromLastYear();
}
