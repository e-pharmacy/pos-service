/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.sales;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.sales.filters.SaleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SalesController {
  private final SalesService salesService;

  @Autowired
  public SalesController(SalesService salesService) {
    this.salesService = salesService;
  }

  @PostMapping("")
  public List<SaleModel> createSales(@RequestBody List<SaleModel> salesList) {
    return salesService.createSales(salesList);
  }

  @GetMapping("/{id}")
  public SaleModel readSaleById(@PathVariable Long id) {
    return salesService.readSaleById(id);
  }

  @GetMapping(params = {"productId"})
  public Integer getProductSalesCount(@RequestParam Long productId) {
    return salesService.getProductSalesCount(productId);
  }

  @GetMapping("")
  public List<SaleModel> readSales(SaleFilter saleFilter) {
    return salesService.readSales(saleFilter);
  }

  @PutMapping("/{id}")
  public SaleModel updateSale(@PathVariable Long id, @RequestBody SaleModel saleModel) {
    return salesService.updateSale(id, saleModel);
  }

  @DeleteMapping("/{id}")
  public SaleModel deleteSale(@PathVariable Long id) {
    return salesService.deleteSale(id);
  }
}