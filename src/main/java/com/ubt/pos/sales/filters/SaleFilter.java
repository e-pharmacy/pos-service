package com.ubt.pos.sales.filters;

import com.ubt.pos.commons.filters.BaseFilter;
import com.ubt.pos.commons.validation.ValidationService;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import java.util.List;

import static com.ubt.pos.jooq.Tables.SALES;

public class SaleFilter extends BaseFilter {
  private List<Long> productIds;
  private Long branchId;

  public List<Long> getProductIds() {
    return productIds;
  }

  public void setProductIds(List<Long> productIds) {
    this.productIds = productIds;
  }

  public Long getBranchId() {
    return branchId;
  }

  public void setBranchId(Long branchId) {
    this.branchId = branchId;
  }

  @Override
  public Condition createFilterCondition() {
    Condition condition = DSL.trueCondition();

    if (ValidationService.notEmpty(getIds())) {
      condition = condition.and(SALES.ID.in(getIds()));
    }
    if (getBranchId() != null) {
      condition = condition.and(SALES.PHARMACY_LOCATION_ID.eq(getBranchId()));
    }
    if (ValidationService.notEmpty(getProductIds())) {
      condition = condition.and(SALES.PRODUCT_ID.in(getProductIds()));
    }

    return condition;
  }
}
