/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.sales.jooqmappers;

import com.ubt.pos.commons.DateTimeService;
import com.ubt.pos.commons.builders.SaleModelBuilder;
import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.jooq.tables.records.SalesRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SaleModelJooqMapper {
  public static SaleModel map(SalesRecord salesRecord) {
    return SaleModelBuilder.aSaleModel()
        .withId(salesRecord.getId())
        .withProductId(salesRecord.getProductId())
        .withCreatedDate(DateTimeService.toLong(salesRecord.getCreatedAt()))
        .withCreatedBy(salesRecord.getCreatedBy())
        .withQuantity(salesRecord.getQuantity())
        .withQuantity(salesRecord.getQuantity())
        .withSellPricePerUnit(salesRecord.getSellPricePerUnit().doubleValue())
        .withPharmacyLocationId(salesRecord.getPharmacyLocationId())
        .build();
  }

  public static SaleModel map(Record record) {
    return SaleModelBuilder.aSaleModel()
        .withId((Long) record.get("id"))
        .withProductId((Long) record.get("product_id"))
        .withCreatedDate(DateTimeService.toLong((LocalDateTime) record.get("created_date")))
        .withCreatedBy((Long) record.get("created_by"))
        .withQuantity((Integer) record.get("quantity"))
        .withSellPricePerUnit((Double) record.get("sell_price_per_unit"))
        .withPharmacyLocationId((Long) record.get("pharmacy_location_id"))
        .build();
  }

  public static SalesRecord unmap(SaleModel saleModel) {
    SalesRecord salesRecord = new SalesRecord();

    if (saleModel.getId() != null) {
      salesRecord.setId(saleModel.getId());
    }

    if (saleModel.getProductId() != null) {
      salesRecord.setProductId(saleModel.getProductId());
    }

    if (saleModel.getCreatedBy() != null) {
      salesRecord.setCreatedBy(saleModel.getCreatedBy());
    }

    if (saleModel.getQuantity() != null) {
      salesRecord.setQuantity(saleModel.getQuantity());
    }

    if (saleModel.getSellPricePerUnit() != null) {
      salesRecord.setSellPricePerUnit(BigDecimal.valueOf(saleModel.getSellPricePerUnit()));
    }

    if (saleModel.getPharmacyLocationId() != null) {
      salesRecord.setPharmacyLocationId(saleModel.getPharmacyLocationId());
    }

    salesRecord.setCreatedAt(DateTimeService.toLocalDateTime(System.currentTimeMillis()));

    return salesRecord;
  }

  public static List<SalesRecord> unmap(List<SaleModel> salesList) {
    List<SalesRecord> salesRecordsList = new ArrayList<>();

    for (SaleModel saleModel : salesList) {
      salesRecordsList.add(unmap(saleModel));
    }

    return salesRecordsList;
  }

  public static List<SaleModel> map(List<SalesRecord> salesRecords) {
    List<SaleModel> sales = new ArrayList<>();

    for (SalesRecord salesRecord : salesRecords) {
      sales.add(map(salesRecord));
    }

    return sales;
  }

  public static List<SaleModel> map(Result<Record> salesRecords) {
    List<SaleModel> sales = new ArrayList<>();

    for (Record salesRecord : salesRecords) {
      sales.add(map(salesRecord));
    }

    return sales;
  }
}
