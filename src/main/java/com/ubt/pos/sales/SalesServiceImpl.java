/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.sales;

import com.ubt.pos.clients.MedicineFeignClient;
import com.ubt.pos.clients.StockFeignClient;
import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.sales.filters.SaleFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesServiceImpl implements SalesService {
  private final SalesRepository salesRepository;
  private final MedicineFeignClient medicineFeignClient;
  private final StockFeignClient stockFeignClient;

  @Autowired
  public SalesServiceImpl(SalesRepository salesRepository, MedicineFeignClient medicineFeignClient, StockFeignClient stockFeignClient) {
    this.salesRepository = salesRepository;
    this.medicineFeignClient = medicineFeignClient;
    this.stockFeignClient = stockFeignClient;
  }

  @Override
  public List<SaleModel> createSales(List<SaleModel> salesList) {
    return salesRepository.createSales(salesList);
  }

  @Override
  public SaleModel readSaleById(Long id) {
    return salesRepository.readSaleById(id);
  }

  @Override
  public List<SaleModel> readSales(SaleFilter saleFilter) {
    return salesRepository.readSales(saleFilter);
  }

  @Override
  public SaleModel updateSale(Long id, SaleModel saleModel) {
    return salesRepository.updateSale(id, saleModel);
  }

  @Override
  public SaleModel deleteSale(Long id) {
    return salesRepository.deleteSale(id);
  }

  @Override
  public Integer getProductSalesCount(Long productId) {
    return salesRepository.getProductSalesCount(productId);
  }
}
