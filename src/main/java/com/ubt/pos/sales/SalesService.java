/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.sales;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.sales.filters.SaleFilter;

import java.util.List;

public interface SalesService {
  SaleModel readSaleById(Long id);

  List<SaleModel> readSales(SaleFilter saleFilter);

  SaleModel updateSale(Long id, SaleModel saleModel);

  SaleModel deleteSale(Long id);

  Integer getProductSalesCount(Long productId);

  List<SaleModel> createSales(List<SaleModel> salesList);
}
