/*
 * Copyright (C) 2021. Smart Bits GmbH. All rights reserved.
 * Created by Arbër Pllana on 29/07/2021.
 */

package com.ubt.pos.sales;

import com.ubt.pos.commons.models.SaleModel;
import com.ubt.pos.jooq.tables.records.SalesRecord;
import com.ubt.pos.sales.filters.SaleFilter;
import com.ubt.pos.sales.jooqmappers.SaleModelJooqMapper;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.ubt.pos.jooq.Tables.SALES;

@Repository
public class SalesRepositoryImpl implements SalesRepository {
  private final DSLContext create;

  @Autowired
  public SalesRepositoryImpl(DSLContext create) {
    this.create = create;
  }

  @Override
  public List<SaleModel> createSales(List<SaleModel> salesList) {
    List<SalesRecord> salesRecordsList = SaleModelJooqMapper.unmap(salesList);

    List<SaleModel> insertedSalesList = new ArrayList<>();
    salesRecordsList.forEach(salesRecord -> {
      SalesRecord insertedSalesRecord = create.insertInto(SALES)
          .set(salesRecord)
          .returning()
          .fetchOne();

      insertedSalesList.add(SaleModelJooqMapper.map(insertedSalesRecord));
    });
    
    return insertedSalesList;
  }

  @Override
  public SaleModel readSaleById(Long id) {
    SalesRecord salesRecord = create.selectFrom(SALES)
        .where(SALES.ID.eq(id))
        .fetchOne();

    return SaleModelJooqMapper.map(salesRecord);
  }

  @Override
  public List<SaleModel> readSales(SaleFilter saleFilter) {
    Condition condition = saleFilter.createFilterCondition();

    List<SalesRecord> salesRecord = create.selectFrom(SALES)
        .where(condition)
        .fetch();

    return SaleModelJooqMapper.map(salesRecord);
  }

  @Override
  public SaleModel updateSale(Long id, SaleModel saleModel) {
    SalesRecord salesRecord = SaleModelJooqMapper.unmap(saleModel);

    SalesRecord updatedSalesRecord = create.update(SALES)
        .set(salesRecord)
        .where(SALES.ID.eq(id))
        .returning()
        .fetchOne();

    return SaleModelJooqMapper.map(updatedSalesRecord);
  }

  @Override
  public SaleModel deleteSale(Long id) {
    SalesRecord salesRecord = create.deleteFrom(SALES)
        .where(SALES.ID.eq(id))
        .returning()
        .fetchOne();

    return SaleModelJooqMapper.map(salesRecord);
  }

  @Override
  public Integer getProductSalesCount(Long productId) {
    return create.select(DSL.sum(SALES.QUANTITY)).from(SALES)
        .where(SALES.PRODUCT_ID.eq(productId))
        .fetchOneInto(int.class);
  }
}
